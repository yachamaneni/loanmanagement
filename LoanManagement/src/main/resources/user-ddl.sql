DROP TABLE Users;

CREATE Table Users(
id numeric(6) primary key,
firstName varchar(30),
lastName varchar(30),
username varchar(30),
password varchar(30),
email varchar(30),
age numeric(6));

select * from Users;

commit