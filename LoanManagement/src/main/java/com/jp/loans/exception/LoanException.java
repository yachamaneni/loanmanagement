package com.jp.loans.exception;

public class LoanException extends Exception {
	private static final long serialVersionUID = -1648287564068549359L;

	public LoanException() {
		super();
	}
	public LoanException(String message) {
		super(message);
	}
}
