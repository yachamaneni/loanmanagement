package com.jp.loans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class LoanManagement_Application {
	public static void main(String[] args) {
		
		SpringApplication.run(LoanManagement_Application.class, args);
		
	}
}

