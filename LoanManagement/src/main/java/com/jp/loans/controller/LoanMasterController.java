package com.jp.loans.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jp.loans.entities.LoanMaster;
import com.jp.loans.exception.LoanException;
import com.jp.loans.service.ILoanMasterService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/loan/loanmaster")
public class LoanMasterController {
	
	@Autowired
	private ILoanMasterService loanMasterService;
	
	@GetMapping(value="/",produces="application/json")
	public List<LoanMaster> getLoanMasterList() throws LoanException{
		return loanMasterService.getLoanMasterList();	
	}
	
	@GetMapping(value="/{loanId}")
	public ResponseEntity<LoanMaster> getLoanId(@PathVariable(value = "loanId") Integer loanId) throws LoanException{
		System.out.println(loanId);
		LoanMaster loanMaster = loanMasterService.getLoanId(loanId);
		return ResponseEntity.ok().body(loanMaster);
	}
	//http://localhost:8082/api/loan/loanmaster/addLoanMaster
    @PostMapping(path="/addLoanMaster")
    public String addLoanMaster(@RequestBody LoanMaster loanmaster) throws LoanException{
    	LoanMaster addedLmaster = loanMasterService.addLoanMaster(loanmaster);
    	if(addedLmaster != null)
    		return "OK";
    	return "Adding User Failed!!";
    }
  //http://localhost:8082/api/loan/loanmaster/5572
  		@PutMapping("/{loanId}")
  		public HttpStatus updateLloanDetails(@PathVariable(value = "loanId") Integer loanId,
  				@Valid @RequestBody LoanMaster loanDetails) throws LoanException{
  			LoanMaster lmaster = loanMasterService.getLoanId(loanId);
  			if(lmaster==null)
  					return HttpStatus.BAD_REQUEST;
  			else{
  				lmaster.setLoanId(loanDetails.getLoanId());
  				
  				return loanMasterService.updateLoanMaster(lmaster)?HttpStatus.ACCEPTED : HttpStatus.BAD_REQUEST;
  				
  			}
  		}
  	//http://localhost:8082/api/loan/loanmaster/deleteUser/5571
  			@DeleteMapping("/deleteUser/{loanId}")
  			public Map<String, Boolean> deleteLoanMasterByLoanId(@PathVariable(value = "loanId") Integer loanId) throws LoanException{
  				LoanMaster master = loanMasterService.getLoanId(loanId);
  				Map<String, Boolean> response = new HashMap<>();
  				if(master!= null){
  					loanMasterService.getLoanId(loanId);
  					response.put("deleted", Boolean.TRUE);
  				}
  				return response;
  			}
  		
  		
}
