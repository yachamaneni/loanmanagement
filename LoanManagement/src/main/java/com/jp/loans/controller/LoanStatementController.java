package com.jp.loans.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jp.loans.entities.LoanStatement;
import com.jp.loans.exception.LoanException;
import com.jp.loans.service.ILoanStatementService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/loan/loanstatement")

public class LoanStatementController {
	
	@Autowired
	private ILoanStatementService loanStatementService;
	@GetMapping(value="/{statementNo}")
	public ResponseEntity<LoanStatement> getStatementNo(@PathVariable(value = "statementNo") Integer statementNo) throws LoanException{
		System.out.println(statementNo);
		LoanStatement loanStatement = loanStatementService.getStatementNo(statementNo);
		return ResponseEntity.ok().body(loanStatement);
	}

}
