package com.jp.loans.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jp.loans.entities.BorrowerMaster;
import com.jp.loans.exception.LoanException;
import com.jp.loans.service.IBorrowerMasterService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/loan/BorrowerMaster")
public class BorrowerController {
	
	@Autowired
	private IBorrowerMasterService borrowerMasterService;
	

	@GetMapping(value="/",produces="application/json")
	public List<BorrowerMaster> getBorrowerMasterList() throws LoanException{
		return borrowerMasterService.getBorrowerMasterList();	
	}
	
	
//http://localhost:8082/api/loan/borrowermaster/id
	@GetMapping(value="/{borrowerId}")
	public ResponseEntity<BorrowerMaster> getBorrowerId(@PathVariable(value = "borrowerId") Integer borrowerId) throws LoanException{
		System.out.println(borrowerId);
		BorrowerMaster borrowerMaster = borrowerMasterService.getBorrowerId(borrowerId);
		return ResponseEntity.ok().body(borrowerMaster);
	}
	
	//http://localhost:8082/api/loan/borrowermaster/addBorrowerMaster
    @PostMapping(path="/addBorrowerMaster")
    public String addBorrowerMaster(@RequestBody BorrowerMaster borrowerMaster) throws LoanException{
    	BorrowerMaster addedBorrower = borrowerMasterService.addBorrowerMaster(borrowerMaster);
    	if(addedBorrower != null)
    		return "OK";
    	return "Adding Borrower Failed!!";
    }
	//http://localhost:8082/api/loan/borrowermaster/id
	@PutMapping("/{borrowerId}")
	public HttpStatus updateBorrowerDetails(@PathVariable(value = "borrowerId") Integer borrowerId,
			@Valid @RequestBody BorrowerMaster borrowerDetails) throws LoanException{
		BorrowerMaster borrower = borrowerMasterService.getBorrowerId(borrowerId);
		if(borrower==null)
				return HttpStatus.BAD_REQUEST;
		else{
			borrower.setBorrowerId(borrowerDetails.getBorrowerId());
/*			borrower.setBorrowername(borrowerDetails.getBorrowername());
			borrower.setBranch(borrowerDetails.getBranch());
			borrower.setAddress(borrowerDetails.getAddress());	
*/			return borrowerMasterService.updateBorrowerMaster(borrower)?HttpStatus.ACCEPTED : HttpStatus.BAD_REQUEST;
			
		}
	}
	//http://localhost:8082/api/loan/borrowermaster/deleteborrower/id
			@DeleteMapping("/deleteborrower/{borrowerId}")
			public Map<String, Boolean> deleteBorrowerMasterByBorrowerId(@PathVariable(value = "borrowerId") Integer borrowerId) throws LoanException{
				BorrowerMaster borrower = borrowerMasterService.getBorrowerId(borrowerId);
				Map<String, Boolean> response = new HashMap<>();
				if(borrower!= null){
					borrowerMasterService.getBorrowerId(borrowerId);
					response.put("deleted", Boolean.TRUE);
				}
				return response;
			}
}
