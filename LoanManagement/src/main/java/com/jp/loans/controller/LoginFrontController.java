package com.jp.loans.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jp.loans.entities.Login;
import com.jp.loans.exception.LoanException;
import com.jp.loans.service.ILoginService;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/loan")
public class LoginFrontController {

@Autowired

private ILoginService loginService;

@ResponseBody
@GetMapping(path="/")
public String greetUser(){
	return "Hello From Spring Boot Rest";	
}

//http://localhost:8082/api/loan/users
	@GetMapping(value="/users",produces="application/json")
	public List<Login> getLoginList() throws LoanException{
		return loginService.getLoginList();	
	}
	
	
//http://localhost:8082/api/loan/users/106
	@GetMapping(value="/users/{userId}")
	public ResponseEntity<Login> getRegisteredUserById(@PathVariable(value = "userId") Integer userId) throws LoanException{
		System.out.println(userId);
		Login login = loginService.getRegisteredUserById(userId);
		return ResponseEntity.ok().body(login);
	}
	
		
		//http://localhost:8082/api/loan/users/addUser
	    @PostMapping(path="/users/addUser")
	    public String addUser(@RequestBody Login loginuser) throws LoanException{
	    	Login addedUser = loginService.addUser(loginuser);
	    	if(addedUser != null)
	    		return "OK";
	    	return "Adding User Failed!!";
	    }
	    
	    
	//http://localhost:8082/api/loan/users/123
		@PutMapping("users/{userId}")
		public HttpStatus updateUser(@PathVariable(value = "userId") Integer userId,
				@Valid @RequestBody Login loginDetails) throws LoanException{
			Login login = loginService.getRegisteredUserById(userId);
			if(login==null)
					return HttpStatus.BAD_REQUEST;
			else{
				login.setUsername(loginDetails.getUsername());
				login.setPassword(loginDetails.getPassword());
				login.setRole(loginDetails.getRole());
				return loginService.updateUser(login)?HttpStatus.ACCEPTED : HttpStatus.BAD_REQUEST;
				
			}
		}
	
		//http://localhost:8082/api/loan/users/deleteUser/123
		@DeleteMapping("users/deleteUser/{userId}")
		public Map<String, Boolean> deleteUser(@PathVariable(value = "userId") Integer userId) throws LoanException{
			Login login = loginService.getRegisteredUserById(userId);
			Map<String, Boolean> response = new HashMap<>();
			if(login!= null){
				loginService.deleteUser(userId);
				response.put("deleted", Boolean.TRUE);
			}
			return response;
		}
}
