package com.jp.loans.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.omg.CORBA.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jp.loans.entities.LenderMaster;
import com.jp.loans.exception.LoanException;
import com.jp.loans.service.ILenderMasterService;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/loan/lendermaster")
public class LenderMasterFrontController {

@Autowired
private ILenderMasterService lenderMasterService;



//http://localhost:8082/api/loan/lendermaster/
	@GetMapping(value="/",produces="application/json")
	public List<LenderMaster> getLenderMasterList() throws LoanException{
		return lenderMasterService.getLenderMasterList();	
	}
	
	
//http://localhost:8082/api/loan/lendermaster/5552
	@GetMapping(value="/{lenderId}")
	public ResponseEntity<LenderMaster> getLenderId(@PathVariable(value = "lenderId") Integer lenderId) throws LoanException{
		System.out.println(lenderId);
		LenderMaster lenderMaster = lenderMasterService.getLenderId(lenderId);
		return ResponseEntity.ok().body(lenderMaster);
	}
	
		
		//http://localhost:8082/api/loan/lendermaster/addLenderMaster
	    @PostMapping(path="/addLenderMaster")
	    public String addLenderMaster(@RequestBody LenderMaster lendermaster) throws LoanException{
	    	LenderMaster addedLmaster = lenderMasterService.addLenderMaster(lendermaster);
	    	if(addedLmaster != null)
	    		return "OK";
	    	return "Adding User Failed!!";
	    }
	    
	    
	//http://localhost:8082/api/loan/lendermaster/5572
		@PutMapping("/{lenderId}")
		public HttpStatus updateLenderDetails(@PathVariable(value = "lenderId") Integer lenderId,
				@Valid @RequestBody LenderMaster lenderDetails) throws LoanException{
			LenderMaster lmaster = lenderMasterService.getLenderId(lenderId);
			if(lmaster==null)
					return HttpStatus.BAD_REQUEST;
			else{
				lmaster.setLenderId(lenderDetails.getLenderId());
				lmaster.setLendername(lenderDetails.getLendername());
				lmaster.setBranch(lenderDetails.getBranch());
				lmaster.setAddress(lenderDetails.getAddress());	
				return lenderMasterService.updateLenderMaster(lmaster)?HttpStatus.ACCEPTED : HttpStatus.BAD_REQUEST;
				
			}
		}
	
		//http://localhost:8082/api/loan/lendermaster/deleteUser/5571
		@DeleteMapping("/deleteUser/{lenderId}")
		public Map<String, Boolean> deleteLenderMasterByLenderId(@PathVariable(value = "lenderId") Integer lenderId) throws LoanException{
			LenderMaster master = lenderMasterService.getLenderId(lenderId);
			Map<String, Boolean> response = new HashMap<>();
			if(master!= null){
				lenderMasterService.getLenderId(lenderId);
				response.put("deleted", Boolean.TRUE);
			}
			return response;
		}
}
