package com.jp.loans.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LOAN_STATEMENT")
@SequenceGenerator(name="LoanStatement_StatementNo_Gen", sequenceName="statement_no_seq")
public class LoanStatement {
	
	@Id
	@GeneratedValue(generator="LoanStatement_StatementNo_Gen")
	@Column(name="STATEMENT_NO")
	private Integer statementNo;
	@Column(name="BORROWER_ID")
	private Integer borrowerId;
	@Column(name="LENDER_ID")
	private Integer lenderId;
	@Column(name="EMI_AMOUNT")
	private float emiAmount;
	@Column(name="INTEREST_AMOUNT")
	private float interestAmount;
	@Column(name="LOAN_AMOUNT")
	private float loanAmount;
	@Column(name="EMI_DATE")
	private Date emiDate;
	@Column(name="LATE_PENALTY")
	private float latePenalty;
	@Column(name="NO_OF_PAYMENTS")
	private Integer numberOfPayments;
	@Column(name="RECEIPT_DATE")
	private Date receiptDate;
	@Column(name="TOTAL_AMOUNT")
	private float totalAmount;
	public LoanStatement(Integer statementNo, Integer borrowerId, Integer lenderId, float emiAmount,
			float interestAmount, float loanAmount, Date emiDate, float latePenalty, Integer numberOfPayments,
			Date receiptDate, float totalAmount) {
		super();
		this.statementNo = statementNo;
		this.borrowerId = borrowerId;
		this.lenderId = lenderId;
		this.emiAmount = emiAmount;
		this.interestAmount = interestAmount;
		this.loanAmount = loanAmount;
		this.emiDate = emiDate;
		this.latePenalty = latePenalty;
		this.numberOfPayments = numberOfPayments;
		this.receiptDate = receiptDate;
		this.totalAmount = totalAmount;
	}
	public LoanStatement() {
		super();
	}
	public Integer getStatementNo() {
		return statementNo;
	}
	public void setStatementNo(Integer statementNo) {
		this.statementNo = statementNo;
	}
	public Integer getBorrowerId() {
		return borrowerId;
	}
	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}
	public Integer getLenderId() {
		return lenderId;
	}
	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}
	public float getEmiAmount() {
		return emiAmount;
	}
	public void setEmiAmount(float emiAmount) {
		this.emiAmount = emiAmount;
	}
	public float getInterestAmount() {
		return interestAmount;
	}
	public void setInterestAmount(float interestAmount) {
		this.interestAmount = interestAmount;
	}
	public float getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(float loanAmount) {
		this.loanAmount = loanAmount;
	}
	public Date getEmiDate() {
		return emiDate;
	}
	public void setEmiDate(Date emiDate) {
		this.emiDate = emiDate;
	}
	public float getLatePenalty() {
		return latePenalty;
	}
	public void setLatePenalty(float latePenalty) {
		this.latePenalty = latePenalty;
	}
	public Integer getNumberOfPayments() {
		return numberOfPayments;
	}
	public void setNumberOfPayments(Integer numberOfPayments) {
		this.numberOfPayments = numberOfPayments;
	}
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	public float getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}
	@Override
	public String toString() {
		return "LoanStatement [statementNo=" + statementNo + ", borrowerId=" + borrowerId + ", lenderId=" + lenderId
				+ ", emiAmount=" + emiAmount + ", interestAmount=" + interestAmount + ", loanAmount=" + loanAmount
				+ ", emiDate=" + emiDate + ", latePenalty=" + latePenalty + ", numberOfPayments=" + numberOfPayments
				+ ", receiptDate=" + receiptDate + ", totalAmount=" + totalAmount + "]";
	}
	
	
}
