package com.jp.loans.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LOAN_MASTER")
@SequenceGenerator(name="LoanMaster_LoanId_Gen", sequenceName="loan_id_seq")
public class LoanMaster {
	@Id
	@GeneratedValue(generator="LoanMaster_LoanId_Gen")
	@Column(name="LOAN_ID")
	private Integer loanId;
	@Column(name="LENDER_ID")
	private Integer lenderId;
	@Column(name="LOAN_NAME")
	private String loanName;
	@Column(name="INTEREST_RATE")
	private float intrestRate;
	@Column(name="LOAN_TENURE")
	private Integer loanTenure;
	@Column(name=" LOAN_TYPE")
	private String loanType;
	public LoanMaster(Integer loanId, Integer lenderId, String loanName, float intrestRate, Integer loanTenure,
			String loanType) {
		super();
		this.loanId = loanId;
		this.lenderId = lenderId;
		this.loanName = loanName;
		this.intrestRate = intrestRate;
		this.loanTenure = loanTenure;
		this.loanType = loanType;
	}
	public LoanMaster() {
		super();
	}
	public Integer getLoanId() {
		return loanId;
	}
	public void setLoanId(Integer loanId) {
		this.loanId = loanId;
	}
	public Integer getLenderId() {
		return lenderId;
	}
	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}
	public String getLoanName() {
		return loanName;
	}
	public void setLoanName(String loanName) {
		this.loanName = loanName;
	}
	public float getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(float intrestRate) {
		this.intrestRate = intrestRate;
	}
	public Integer getLoanTenure() {
		return loanTenure;
	}
	public void setLoanTenure(Integer loanTenure) {
		this.loanTenure = loanTenure;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	@Override
	public String toString() {
		return "LoanMaster [loanId=" + loanId + ", lenderId=" + lenderId + ", loanName=" + loanName + ", intrestRate="
				+ intrestRate + ", loanTenure=" + loanTenure + ", loanType=" + loanType + "]";
	}

}
