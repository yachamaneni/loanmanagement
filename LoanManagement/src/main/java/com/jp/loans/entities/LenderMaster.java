package com.jp.loans.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LENDER_MASTER")
@SequenceGenerator(name="Lender_Master_LenderId_Gen", sequenceName="lender_id_seq")
public class LenderMaster {
	
	@Id
	@GeneratedValue(generator="Lender_Master_LenderId_Gen")
	@Column(name="LENDER_ID")
	private Integer lenderId;
	/*@Column(name="USER_ID")
	private Integer userId;*/
	@Column(name="LENDER_NAME")
	private String lendername;
	@Column(name="BRANCH")
	private String branch;
	@Column(name="ADDRESS")
	private String address;
	
	//Association with login
	@OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="USER_ID")
	private Login login;
	
	
	public Integer getLenderId() {
		return lenderId;
	}
	public void setLenderId(Integer lenderId) {
		this.lenderId = lenderId;
	}
/*	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}*/
	public String getLendername() {
		return lendername;
	}
	public void setLendername(String lendername) {
		this.lendername = lendername;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	/*@Override
	public String toString() {
		return "LenderMaster [lenderId=" + lenderId + ", userId=" + userId + ", lendername=" + lendername + ", branch="
				+ branch + ", address=" + address + "]";
	}*/
	/*public LenderMaster(Integer lenderId, Integer userId, String lendername, String branch, String address) {
		super();
		this.lenderId = lenderId;
		this.userId = userId;
		this.lendername = lendername;
		this.branch = branch;
		this.address = address;
	}*/
	public LenderMaster() {
		super();
	}
	public LenderMaster(Integer lenderId, String lendername, String branch, String address) {
		super();
		this.lenderId = lenderId;
		this.lendername = lendername;
		this.branch = branch;
		this.address = address;
	}
	@Override
	public String toString() {
		return "LenderMaster [lenderId=" + lenderId + ", lendername=" + lendername + ", branch=" + branch + ", address="
				+ address + "]";
	}
	
		
}
