package com.jp.loans.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LOGIN")
@SequenceGenerator(name="Login_UserId_Gen", sequenceName="user_id_seq")
public class Login {
	
	@Id
	@GeneratedValue(generator="Login_UserId_Gen")
	@Column(name="USER_ID")
	private Integer userId;
	@Column(name="USERNAME")
	private String username;
	@Column(name="PASSWORD")
	private String password;
	@Column(name="ROLE")
	private String role;
	
	public Login(String username, String password, Integer userId, String role) {
		super();
		this.username = username;
		this.password = password;
		this.userId = userId;
		this.role = role;
	}

	public Login(String username, String password, String role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public Login() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getUserId() {
		return userId;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Login [username=" + username + ", password=" + password + ", userId=" + userId + ", role=" + role + "]";
	}
	
	
	
}
