package com.jp.loans.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="BORROWER_MASTER")
@SequenceGenerator(name="BorrowerMaster_BorrowerId_Gen", sequenceName="borrower_id_seq")
public class BorrowerMaster {

	@Id
	@GeneratedValue(generator="BorrowerMaster_BorrowerId_Gen")
	@Column(name="BORROWER_ID")
	private Integer borrowerId;
	@Column(name="USER_ID")
	private Integer userId;
	@Column(name="BORROWER_NAME")
	private String borrowerName;
	@Column(name="GENDER")
	private String gender;
	@Column(name="ADDRESS")
	private String address;
	@Column(name="CITY")
	private String city;
	@Column(name="MOBILENO")
	private Long mobileNo;
	@Column(name="SALARY")
	private float salary;
	@Column(name="DESIGNATION")
	private String designation;
	@Column(name="EMAILID")
	private String emailId;
	@Column(name="PAN")
	private String pan;
	@Column(name="DOB")
	private Date dob;
	public Integer getBorrowerId() {
		return borrowerId;
	}
	public void setBorrowerId(Integer borrowerId) {
		this.borrowerId = borrowerId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getBorrowerName() {
		return borrowerName;
	}
	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Long getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public BorrowerMaster(Integer borrowerId, Integer userId, String borrowerName, String gender, String address,
			String city, Long mobileNo, float salary, String designation, String emailId, String pan, Date dob) {
		super();
		this.borrowerId = borrowerId;
		this.userId = userId;
		this.borrowerName = borrowerName;
		this.gender = gender;
		this.address = address;
		this.city = city;
		this.mobileNo = mobileNo;
		this.salary = salary;
		this.designation = designation;
		this.emailId = emailId;
		this.pan = pan;
		this.dob = dob;
	}
	public BorrowerMaster() {
		super();
	}
	@Override
	public String toString() {
		return "BorrowerMaster [borrowerId=" + borrowerId + ", userId=" + userId + ", borrowerName=" + borrowerName
				+ ", gender=" + gender + ", address=" + address + ", city=" + city + ", mobileNo=" + mobileNo
				+ ", salary=" + salary + ", designation=" + designation + ", emailId=" + emailId + ", pan=" + pan
				+ ", dob=" + dob + "]";
	}
	
	
}
