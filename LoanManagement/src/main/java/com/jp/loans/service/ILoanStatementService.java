package com.jp.loans.service;

import com.jp.loans.entities.LoanStatement;
import com.jp.loans.exception.LoanException;

public interface ILoanStatementService {
	public LoanStatement getStatementNo(Integer statementNo) throws LoanException;
	/*public List<LoanStatement> getLoanStatementList() throws LoanException;
	public boolean deleteLoanStatementByStatementNo(Integer statementNo)throws LoanException;
	public LoanStatement addLoanStatement(LoanStatement loanStatement) throws LoanException;
	public boolean updateLoanStatement(LoanStatement loanStatement)throws LoanException;*/
}
