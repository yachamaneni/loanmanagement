package com.jp.loans.service;

import java.util.List;

import com.jp.loans.entities.LoanMaster;
import com.jp.loans.exception.LoanException;

public interface ILoanMasterService {

	public LoanMaster getLoanId(Integer loanId) throws LoanException;
	public List<LoanMaster> getLoanMasterList() throws LoanException;
	public boolean deleteLoanMasterByLoanId(Integer loanId)throws LoanException;
	public LoanMaster addLoanMaster(LoanMaster loanId) throws LoanException;
	public boolean updateLoanMaster(LoanMaster loanId)throws LoanException;
}
