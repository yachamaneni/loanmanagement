package com.jp.loans.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jp.loans.dao.ILoanStatementDao;
import com.jp.loans.entities.LoanStatement;
import com.jp.loans.exception.LoanException;
@Service
public class LoanStatementService implements ILoanStatementService {

	@Autowired
	private ILoanStatementDao loanStatementDao;
	@Override
	public LoanStatement getStatementNo(Integer statementNo) throws LoanException {
		return loanStatementDao.getStatementNo(statementNo);
	}

}
