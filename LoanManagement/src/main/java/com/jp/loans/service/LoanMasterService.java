package com.jp.loans.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jp.loans.dao.ILoanMasterDao;
import com.jp.loans.entities.LoanMaster;
import com.jp.loans.exception.LoanException;

@Service
public class LoanMasterService implements ILoanMasterService {
	@Autowired
	private ILoanMasterDao loanMasterDao;

	@Override
	public LoanMaster getLoanId(Integer loanId) throws LoanException {
		return loanMasterDao.getLoanId(loanId);
	}

	@Override
	public List<LoanMaster> getLoanMasterList() throws LoanException {
		return loanMasterDao.getLoanMasterList();
	}

	@Override
	public boolean deleteLoanMasterByLoanId(Integer loanId) throws LoanException {
		return loanMasterDao.deleteLoanMasterByLoanId(loanId);
	}

	@Override
	public LoanMaster addLoanMaster(LoanMaster loanId) throws LoanException {
		return loanMasterDao.addLoanMaster(loanId);
	}

	@Override
	public boolean updateLoanMaster(LoanMaster loanId) throws LoanException {
		return loanMasterDao.updateLoanMaster(loanId);
	}

}
