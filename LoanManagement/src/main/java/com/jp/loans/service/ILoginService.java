package com.jp.loans.service;

import java.util.List;

import com.jp.loans.entities.Login;
import com.jp.loans.exception.LoanException;

public interface ILoginService {
	public Login getRegisteredUserById(Integer userId) throws LoanException;
	public List<Login> getLoginList() throws LoanException;
	public Login addUser(Login loginuser) throws LoanException;
	public boolean deleteUser(Integer userId)throws LoanException;
	public boolean updateUser(Login login)throws LoanException;
}
