package com.jp.loans.service;

import java.util.List;

import com.jp.loans.entities.BorrowerMaster;
import com.jp.loans.exception.LoanException;

public interface IBorrowerMasterService {

	public BorrowerMaster getBorrowerId(Integer borrowerId) throws LoanException;
	public List<BorrowerMaster> getBorrowerMasterList() throws LoanException;
	public boolean deleteBorrowerMasterByBorrowerId(Integer borrowerId)throws LoanException;
	public BorrowerMaster addBorrowerMaster(BorrowerMaster borrowerMaster) throws LoanException;
	public boolean updateBorrowerMaster(BorrowerMaster borrowerMaster)throws LoanException;
}
