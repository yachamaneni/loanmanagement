package com.jp.loans.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jp.loans.dao.IBorrowerMasterDao;
import com.jp.loans.entities.BorrowerMaster;
import com.jp.loans.exception.LoanException;

@Service
public class BorrowerMasterService implements IBorrowerMasterService {

	@Autowired
	private IBorrowerMasterDao borrowerMasterDao;
	
	@Override
	public BorrowerMaster getBorrowerId(Integer borrowerId) throws LoanException {
		return borrowerMasterDao.getBorrowerId(borrowerId);
	}

	@Override
	public List<BorrowerMaster> getBorrowerMasterList() throws LoanException {
		return borrowerMasterDao.getBorrowerMasterList();
	}

	@Override
	public boolean deleteBorrowerMasterByBorrowerId(Integer borrowerId) throws LoanException {
		return borrowerMasterDao.deleteBorrowerMasterByBorrowerId(borrowerId);
	}

	@Override
	public BorrowerMaster addBorrowerMaster(BorrowerMaster borrowerMaster) throws LoanException {
		return borrowerMasterDao.addBorrowerMaster(borrowerMaster);
	}

	@Override
	public boolean updateBorrowerMaster(BorrowerMaster borrowerMaster) throws LoanException {
		return borrowerMasterDao.updateBorrowerMaster(borrowerMaster);
	}

}
