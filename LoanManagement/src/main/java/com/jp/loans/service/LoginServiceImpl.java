package com.jp.loans.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jp.loans.dao.ILoginDao;
import com.jp.loans.entities.Login;
import com.jp.loans.exception.LoanException;
@Service
public class LoginServiceImpl implements ILoginService {
	@Autowired
	private ILoginDao loginDao;

	@Override
	public Login getRegisteredUserById(Integer userId) throws LoanException {
		return loginDao.getRegisteredUserById(userId);
		
	}

	@Override
	public List<Login> getLoginList() throws LoanException {
		return loginDao.getLoginList();
	}
	@Transactional
	@Override
	public Login addUser(Login loginuser) throws LoanException {
		return loginDao.addUser(loginuser);
	}

	@Transactional
	@Override
	public boolean deleteUser(Integer userId) throws LoanException {
		return loginDao.deleteUser(userId);
	}

	@Transactional
	@Override
	public boolean updateUser(Login login) throws LoanException {	
		return loginDao.updateUser(login);
	}

}
