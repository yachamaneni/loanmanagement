package com.jp.loans.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jp.loans.dao.ILenderMasterDao;
import com.jp.loans.entities.LenderMaster;
import com.jp.loans.exception.LoanException;
@Service
public class LenderMasterServiceImpl implements ILenderMasterService {
	@Autowired
	private ILenderMasterDao lenderMasterDao;

	@Override
	public LenderMaster getLenderId(Integer lenderId) throws LoanException {
		return lenderMasterDao.getLenderId(lenderId);
	}

	@Override
	public List<LenderMaster> getLenderMasterList() throws LoanException {	
		return lenderMasterDao.getLenderMasterList();
	}
	@Transactional
	@Override
	public boolean deleteLenderMasterByLenderId(Integer lenderId) throws LoanException {
		return lenderMasterDao.deleteLenderMasterByLenderId(lenderId);
	}
	@Transactional
	@Override
	public LenderMaster addLenderMaster(LenderMaster lenderMaster) throws LoanException {
		return lenderMasterDao.addLenderMaster(lenderMaster);
	}
	@Transactional
	@Override
	public boolean updateLenderMaster(LenderMaster lenderMaster) throws LoanException {
		return lenderMasterDao.updateLenderMaster(lenderMaster);
	}

}
