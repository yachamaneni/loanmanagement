package com.jp.loans.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.jp.loans.entities.LoanMaster;
import com.jp.loans.exception.LoanException;

@Repository
public class LoanMasterDao implements ILoanMasterDao {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LoanMaster getLoanId(Integer loanId) throws LoanException {
		LoanMaster master=entityManager.find(LoanMaster.class,loanId);
		return master;
	}

	@Override
	public List<LoanMaster> getLoanMasterList() throws LoanException {
		Query query = entityManager.createQuery("from LoanMaster");
		return query.getResultList();
	}

	@Override
	public boolean deleteLoanMasterByLoanId(Integer loanId) throws LoanException {
		LoanMaster master = getLoanId(loanId);
		if(master==null)
		return false;
		else{
			entityManager.remove(master);
			return true;
		}
	}

	@Override
	public LoanMaster addLoanMaster(LoanMaster loanId) throws LoanException {
		System.out.println("LoanMaster Added in Database : "+loanId);
		return loanId;
	}

	@Override
	public boolean updateLoanMaster(LoanMaster loanId) throws LoanException {
		return entityManager.merge(loanId)!=null;
	}

}
