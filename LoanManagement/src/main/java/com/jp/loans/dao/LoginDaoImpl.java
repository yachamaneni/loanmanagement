package com.jp.loans.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.jp.loans.entities.Login;
import com.jp.loans.exception.LoanException;
@Repository
public class LoginDaoImpl implements ILoginDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Login getRegisteredUserById(Integer userId) throws LoanException {
		Login login = entityManager.find(Login.class, userId);
		return login;
	}

	@Override
	public List<Login> getLoginList() throws LoanException {
		Query query = entityManager.createQuery("from Login");
		return query.getResultList();
	}

	@Override
	public Login addUser(Login loginuser)throws LoanException {
		System.out.println("User Added in Database : "+loginuser);
		return loginuser;
	}

	@Override
	public boolean deleteUser(Integer userId) throws LoanException {
		Login login = getRegisteredUserById(userId);
		if(login==null)
		return false;
		else{
			entityManager.remove(login);
			return true;
		}
	}

	@Override
	public boolean updateUser(Login login) throws LoanException {
		return entityManager.merge(login)!=null;
	}

}
