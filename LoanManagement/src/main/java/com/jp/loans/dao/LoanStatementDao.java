package com.jp.loans.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.jp.loans.entities.LoanStatement;
import com.jp.loans.exception.LoanException;
@Repository
public class LoanStatementDao implements ILoanStatementDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public LoanStatement getStatementNo(Integer statementNo) throws LoanException {
		LoanStatement statement=entityManager.find(LoanStatement.class,statementNo);
		return statement;
	}

	/*@Override
	public List<LoanStatement> getLoanStatementList() throws LoanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteLoanStatementByStatementNo(Integer statementNo) throws LoanException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public LoanStatement addLoanStatement(LoanStatement loanStatement) throws LoanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateLoanStatement(LoanStatement loanStatement) throws LoanException {
		// TODO Auto-generated method stub
		return false;
	}*/

}
