package com.jp.loans.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.jp.loans.entities.LenderMaster;
import com.jp.loans.exception.LoanException;
@Repository
public class LenderMasterDaoImpl implements ILenderMasterDao {
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Override
	public LenderMaster getLenderId(Integer lenderId) throws LoanException {
		LenderMaster master=entityManager.find(LenderMaster.class,lenderId);
		return master;
	}


	@Override
	public List<LenderMaster> getLenderMasterList() throws LoanException {
		Query query = entityManager.createQuery("from LenderMaster");
		return query.getResultList();
	}


	@Override
	public boolean deleteLenderMasterByLenderId(Integer lenderId) throws LoanException {
		LenderMaster master = getLenderId(lenderId);
		if(master==null)
		return false;
		else{
			entityManager.remove(master);
			return true;
		}
	}


	@Override
	public LenderMaster addLenderMaster(LenderMaster lenderMaster) throws LoanException {
		System.out.println("LenderMaster Added in Database : "+lenderMaster);
		return lenderMaster;
	}


	@Override
	public boolean updateLenderMaster(LenderMaster lenderMaster) throws LoanException {
		return entityManager.merge(lenderMaster)!=null;
	}


	
}
