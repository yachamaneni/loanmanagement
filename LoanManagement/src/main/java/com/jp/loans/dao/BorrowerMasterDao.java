package com.jp.loans.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.jp.loans.entities.BorrowerMaster;
import com.jp.loans.exception.LoanException;

@Repository
public class BorrowerMasterDao implements IBorrowerMasterDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public BorrowerMaster getBorrowerId(Integer borrowerId) {
		BorrowerMaster borrower=entityManager.find(BorrowerMaster.class, borrowerId);
		return borrower;
	}

	@Override
	public List<BorrowerMaster> getBorrowerMasterList() throws LoanException {
		Query query = entityManager.createQuery("from BorrowerMaster");
		return query.getResultList();
	}

	@Override
	public boolean deleteBorrowerMasterByBorrowerId(Integer borrowerId) throws LoanException {
		BorrowerMaster borrower= getBorrowerId(borrowerId);
		if(borrower==null)
		{
			return false;
		}
			else{
				entityManager.remove(borrower);
				return true;
			}
		}
	

	@Override
	public BorrowerMaster addBorrowerMaster(BorrowerMaster borrowerMaster) throws LoanException {
		System.out.println("BorrowerMaster Added in Database : "+borrowerMaster);
		return borrowerMaster;
	}

	@Override
	public boolean updateBorrowerMaster(BorrowerMaster borrowerMaster) throws LoanException {
		return entityManager.merge(borrowerMaster)!=null;
	}

}
