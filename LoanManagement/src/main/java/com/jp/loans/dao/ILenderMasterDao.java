package com.jp.loans.dao;

import java.util.List;

import com.jp.loans.entities.LenderMaster;
import com.jp.loans.exception.LoanException;

public interface ILenderMasterDao {

	public LenderMaster getLenderId(Integer lenderId) throws LoanException;
	public List<LenderMaster> getLenderMasterList() throws LoanException;
	public boolean deleteLenderMasterByLenderId(Integer lenderId)throws LoanException;
	public LenderMaster addLenderMaster(LenderMaster lenderMaster) throws LoanException;
	public boolean updateLenderMaster(LenderMaster lenderMaster)throws LoanException;
}
